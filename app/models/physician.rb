class Physician < ApplicationRecord
  has_many :appointments
  has_many :patients, through: :appointments
  has_many :time_slots

  validates :specialty,
            :work_time, presence: true

  serialize :work_time, Hash
end

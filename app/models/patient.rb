class Patient < ApplicationRecord
  has_many :appointments
  has_many :physicians, through: :appointments

  validates :first_name,
            :last_name,
            :date_of_birth, presence: true

  def full_name
    full_name = "#{self.last_name} #{self.first_name}"
    full_name += " #{self.patronymic}" if self.patronymic

    full_name
  end

  def short_date_of_birth
    date_of_birth.strftime('%d.%m.%Y')
  end
end

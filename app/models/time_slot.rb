class TimeSlot < ApplicationRecord
  belongs_to :physician
  belongs_to :appointment, optional: true

  validates :date, :time, :physician, presence: true

  def date_time
    "#{date.strftime('%d.%m.%Y')}, #{time}"
  end
end

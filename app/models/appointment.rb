class Appointment < ApplicationRecord
  belongs_to :patient
  belongs_to :physician

  validates :date,
            :patient,
            :physician, presence: true

  def date_time
    date.strftime('%d.%m.%Y, %H:%M')
  end
end

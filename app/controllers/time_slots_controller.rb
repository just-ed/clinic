class TimeSlotsController < ApplicationController
  def index
    @time_slots = TimeSlot.where(physician_id: params[:physician_id])
    @time_slots_days = @time_slots.pluck(:date).uniq
  end

  def daily_slots
    @time_slots = TimeSlot.where(physician_id: params[:physician_id])
    @daily_slots = @time_slots.where(date: params[:date])
  end

  private

  def time_slot_params
    params.require(:time_slot).permit(:date, :time, :taken, :physician_id, :appointment_id)
  end
end

class PatientsController < ApplicationController
  before_action :set_patient, except: %i[index new create]

  def index
    @patients = Patient.all
  end

  def show
  end

  def new
    @patient = Patient.new
  end

  def edit
  end

  def create
    @patient = Patient.new(patient_params)

    if @patient.save
      redirect_to @patient, notice: 'Patient was successfully created.'
    else
      flash.now[:error] = @patient.errors.full_messages.join(', ')
      render :new
    end
  end

  def update
    if @patient.update(patient_params)
      redirect_to @patient, notice: 'Patient was successfully updated.'
    else
      flash.now[:error] = @patient.errors.full_messages.join(', ')
      render :edit
    end
  end

  private

  def set_patient
    @patient = Patient.find(params[:id])
  end

  def patient_params
    params.require(:patient).permit(:first_name, :last_name, :patronymic, :date_of_birth)
  end
end
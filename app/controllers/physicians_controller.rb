class PhysiciansController < ApplicationController
  before_action :set_physician, except: %i[index new create]

  def index
    @physicians = Physician.all
  end

  def show
  end

  def new
    @physician = Physician.new
  end

  def edit
  end

  def create
    @physician = Physician.new(physician_params)

    if @physician.save
      redirect_to @physician, notice: 'Physician was successfully created.'
    else
      flash.now[:error] = @physician.errors.full_messages.join(', ')
      render :new
    end
  end

  def update
    if @physician.update(patient_params)
      redirect_to @physician, notice: 'Physician was successfully updated.'
    else
      flash.now[:error] = @physician.errors.full_messages.join(', ')
      render :edit
    end
  end

  private

  def set_physician
    @physician = Physician.find(params[:id])
  end

  def physician_params
    params.require(:physician).permit(:specialty, :work_time)
  end
end
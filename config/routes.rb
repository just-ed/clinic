Rails.application.routes.draw do
  root "appointments#index"

  resources :appointments, except: [:new]
  resources :patients, except: [:destroy]

  resources :physicians, except: [:destroy] do
    resources :time_slots, only: [:index] do
      get 'appointments/new', to: 'appointments#new'
    end

    get 'time_slots/:date', to: 'time_slots#daily_slots', as: 'daily_slots'
  end
end

class CreatePhysicians < ActiveRecord::Migration[5.2]
  def change
    create_table :physicians do |t|
      t.string :specialty, null: false
      t.text :work_time

      t.timestamps
    end
  end
end

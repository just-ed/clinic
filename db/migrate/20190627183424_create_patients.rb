class CreatePatients < ActiveRecord::Migration[5.2]
  def change
    create_table :patients do |t|
      t.string :first_name, null: false
      t.string :last_name, null: false
      t.string :patronymic
      t.date :date_of_birth, null:false

      t.timestamps
    end
  end
end

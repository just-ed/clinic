class CreateTimeSlots < ActiveRecord::Migration[5.2]
  def change
    create_table :time_slots do |t|
      t.date :date
      t.string :time
      t.references :physician, foreign_key: true
      t.references :appointment, foreign_key: true

      t.timestamps
    end
  end
end

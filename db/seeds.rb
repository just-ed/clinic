# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Patient.create(first_name: 'Mike', last_name: 'Green', date_of_birth: '01.05.1996')
Patient.create(first_name: 'John', last_name: 'White', date_of_birth: '08.11.1974')

ph1 = Physician.create(specialty: 'therapist', work_time: { "monday" => ["09:00", "15:00"], "tuesday" => ["14:00", "17:00"]})
ph2 = Physician.create(specialty: 'immunologist', work_time: { "sunday" => ["10:00", "12:00"], "wednesday" => ["11:00", "16:00"]})

TimeSlot.create(physician: ph1, date: Date.today, time: "09:00")
TimeSlot.create(physician: ph1, date: Date.today, time: "11:00")

TimeSlot.create(physician: ph1, date: Date.tomorrow, time: "09:00")
TimeSlot.create(physician: ph1, date: Date.tomorrow, time: "11:00")

TimeSlot.create(physician: ph2, date: Date.today, time: "12:00")
TimeSlot.create(physician: ph2, date: Date.today, time: "14:00")